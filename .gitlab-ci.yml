# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0 filetype=yaml:

.templates_sha: &template_sha 0c312d9c7255f46e741d43bcd1930f09cd12efe7

include:
  # Fedora container builder template
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/fedora.yml'

stages:
  - prep             # prep work like rebuilding the container images if there is a change
  - build            # for actually building and testing things in a container
  - test-suite       # for running the test suite in a VM
  - test-suite-no-libwacom # for running the test suite in a VM (libwacom disabled)
  - valgrind         # for running the test suite under valgrind in a VM
  - distro           # distribs test
  - deploy           # trigger wayland's website generation
  - container_clean  # clean up unused container images (scheduled jobs only)

variables:
  FEDORA_PACKAGES: |
    git-core gcc gcc-c++ pkgconf-pkg-config meson
    xorg-x11-server-devel xorg-x11-util-macros
    libX11-devel libXi-devel libXrandr-devel libXinerama-devel
    systemd systemd-devel libevdev-devel
    pytest python3-attrs python3-libevdev python3-pyyaml python3-gobject-devel
    glib2-devel gobject-introspection-devel
  MESON_BUILDDIR: "builddir"

  # these tags should be updated each time the list of packages is updated
  # changing these will force rebuilding the associated image
  # The actual value is meaningless, just for human consumption
  FEDORA_TAG:  '2022-06-08.3'
  FEDORA_VERSION: 36

.policy:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  # cancel run when a newer version is pushed to the branch
  interruptible: true
  dependencies: []


.default_artifacts:
  artifacts:
    name: "meson-logs-$CI_JOB_NAME"
    when: always
    expire_in: 1 week
    paths:
      - $MESON_BUILDDIR/meson-logs
      - console.out
    reports:
      junit: $MESON_BUILDDIR/testlog.junit.xml


qemu-prep:
  extends:
    - .fdo.qemu-build@fedora
    - .policy
  stage: prep
  tags:
    - kvm
  variables:
    GIT_STRATEGY: none
    FDO_DISTRIBUTION_VERSION: $FEDORA_VERSION
    FDO_DISTRIBUTION_TAG: $FEDORA_TAG
    FDO_DISTRIBUTION_PACKAGES: $FEDORA_PACKAGES

vm-test:
  extends:
    - .policy
    - .default_artifacts
    - .fdo.distribution-image@fedora
  tags:
    - kvm
  stage: build
  script:
    # start our vm, no args required
    - /app/vmctl start || (echo "Error - Failed to start the VM." && exit 1)
    - "scp -r $PWD vm:"
    - echo "CI_JOB_ID=\"$CI_JOB_ID\"" > sshenv
    - echo "CI_JOB_NAME=\"$CI_JOB_NAME\"" >> sshenv
    - echo "CI_PROJECT_NAME=\"$CI_PROJECT_NAME\"" >> sshenv
    - echo "MESON_ARGS=\"$MESON_ARGS\"" >> sshenv
    - echo "MESON_BUILDDIR=\"$MESON_BUILDDIR\"" >> sshenv
    - echo "MESON_TEST_ARGS=\"$MESON_TEST_ARGS\"" >> sshenv
    - echo "NINJA_ARGS=\"$NINJA_ARGS\"" >> sshenv
    - "scp sshenv vm:~/$CI_PROJECT_NAME/.meson_environment"
    - /app/vmctl exec "cd $CI_PROJECT_NAME ; git clone https://github.com/linuxwacom/xf86-input-wacom"
    - /app/vmctl exec "cd $CI_PROJECT_NAME ; .gitlab-ci/meson-build.sh xf86-input-wacom" && touch .success || true
    # no matter the results of the tests, we want to fetch the logs
    - scp -r vm:"$CI_PROJECT_NAME/xf86-input-wacom/$MESON_BUILDDIR" .
    - /app/vmctl stop

    - if [[ ! -e .success ]] ;
      then
        exit 1 ;
      fi
  variables:
    FDO_DISTRIBUTION_VERSION: $FEDORA_VERSION
    FDO_DISTRIBUTION_TAG: $FEDORA_TAG
    MESON_ARGS: -Dunittests=enabled -Dwacom-gobject=enabled
